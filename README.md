# t-ceremony-latex: 継続的執筆環境(LaTeX) A Continuous Publishing system #

### What is this repository for? ###

Bitbucket と LaTeX と wercker を使って、継続的にTeXの執筆を行うための仕組みです。BitbucketにTeXのファイルをコミットすると、自動的にコンパイルされ、出力先のリポジトリにpdfファイルが置かれます。
  
### How do I get set up? ###
次のステップで設定してください。最初に設定すれば次からはcommitすれば自動でリポジトリに出力します。

* このリポジトリの中身を自分のリポジトリにコピーして下さい。
* src ディレクトリにTeXのファイルを入れてください。
* Bitbucket にpushしてください。
* コンパイルしたpdfを置くための別のリポジトリをBitbucketに作成して下さい。
* wercker にTeXのファイルを置いたリポジトリを登録して下さい。
* wercker のデプロイターゲットをカスタムデプロイで追加してください。
    * Deploy target name : 適当な名前（利用してません）
    * Auto deploy：チェックする
    * branch(es): master
* Deploy pipelineの変数を追加してください。
    * username: Bitbucket アカウント名
    * password: Bitbucket パスワード
    * email: e-mail アドレス
    * deploy_owner: コンパイルしたpdfを置くリポジトリのオーナー名（チーム名やアカウント名）
    * deploy_repository:  コンパイルしたpdfを置くリポジトリ名

本プロジェクトの場合、pdf を出力するリポジトリと、werckerのプロジェクトは次のサイトですので、参考にしてください。

* [bitbucket: imagire/latex-production](https://bitbucket.org/imagire/latex-production)
* [wercker: imagire/LATEX](https://app.wercker.com/#applications/54334562dca462c70f0004b5)

### 注意 ###

* 私が作ったwerckerのbox(imagire/latex@#.#.#)を利用したくない場合は、wercker_full.yml を wercker.yml に差し替えて使ってください。（コンパイル時間はかかります）
* ファイルは、UTF-8で保存してください。

### What is this repository for? ###

This project comes true a continuous publishing in free using Bitbucket and LaTeX and wercker. When tex files are committed in a Bitbucket repository, CI makes a complied pdf and ships to another Bitbucket repository.

### How do I get set up? ###

* [bitbucket: imagire/latex-production](https://bitbucket.org/imagire/latex-production)
* [wercker: imagire/LATEX](https://app.wercker.com/#applications/54334562dca462c70f0004b5)

Set up as below, once you set up your wercker, pdf will publish automatically when you commit new sources to your repository

* download this repo's files.
* write tex files in the src directory.
* push to your Bitbucket repository.
* create another Bitbucket repository for deploy.
* add app in wercker in terms for your 1st repository.
* set variables for your app.
    * username: your Bitbucket account
    * password: Bitbucket's password
    * email: e-mail address for committing on git 
    * deploy_owner: the owner of the 2nd repository in Bitbucket
    * deploy_repository: 2nd repository's name
* set deploy target to 2nd repository

output repository and wercker project for this repository is below, We hope this helps.

* [bitbucket: imagire/latex-production](https://bitbucket.org/imagire/latex-production)
* [wercker: imagire/LATEX](https://app.wercker.com/#applications/54334562dca462c70f0004b5)

### Note ###

* If you want's use my box(imagire/latex@#.#.#), please use wercker_full.yml with rename as wercker.yml.
* Save your files in UTF-8.

### Contribution guidelines ###

* Writing tests: please write if possible.

* Code review: we will review your pull requests.

### Who do I talk to? ###

* Repo owner: Takashi Imagire

### Rreference ###

* @takahashim: [[ReVIEW Tips] DockerでRe:VIEW](http://qiita.com/takahashim/items/406421d515ef1d4f1189)
